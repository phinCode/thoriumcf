<cfinclude template="../_templates/header.cfm">
<cfinclude template="../_templates/nav.cfm">
<!--- ======================================= --->
<cfinclude template="controller.cfm">
<cfoutput>

	
	    <div class="content-page">
	
		<!-- Start content -->
        <div class="content">
            
			<div class="container-fluid">

					
				<div class="row">
						<div class="col-xl-12">
								<div class="breadcrumb-holder">
										<h1 class="main-title float-left">Users</h1>
										<ol class="breadcrumb float-right">
											<li class="breadcrumb-item">Dashboard</li>
											<li class="breadcrumb-item active">Users</li>
										</ol>
										<div class="clearfix"></div>
								</div>
						</div>
				</div>
				<!-- end row -->

							
				<div class="row">
					<div class="col-xl-12">									
						<div class="table-responsive-sm">
					<table class="table">
						<thead>
							<tr>
							  <th scope="col">First</th>
							  <th scope="col">Last</th>
							  <th scope="col">Email</th>
							  <th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
				
					  	<cfloop query="records_users">
						  	<tr>
							  <td>#records_users.u_name_first#</td>
							  <td>#records_users.u_name_last#</td>
							  <td>#records_users.u_email#</td>
							  <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-#records_users.user_id#">Large modal</button></td>
							</tr>
							
							<div class="modal fade bd-#records_users.user_id#" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-lg">
								<div class="modal-content">
								  
									<div class="container m-5">
										<div class="row">

											<form action="index.cfm" method="post">
												<div class="form-row">
													<div class="form-group col-md-6">
													  <input value="#records_users.u_name_first#" type="text" class="form-control" placeholder="First Name">
													</div>
													<div class="form-group col-md-6">
													  <input value="#records_users.u_name_last#" type="text" class="form-control" placeholder="Last Name">
													</div>
												</div>
												
												<div class="form-row">
													<div class="form-group col-md-12">
														<input name="user_id" value="#records_users.user_id#" type="hidden">
														<input name="update" value="Save" type="submit" class="btn btn-success btn-sm">
													</div>
												</div>
											</form>

									  	</div>
									</div>
									
								</div>
							  </div>
							</div>
					
						</cfloop>

						</tbody>
					</table>
					</div>
				</div>



            </div>
			<!-- END container-fluid -->

		</div>
		<!-- END content -->

    </div>
	<!-- END content-page -->	
			
<!---	
	<cfif isDefined('session.show_message')>#session.show_message#</cfif>
	
	<div class="container">
		<div class="row">
			<div class="col-12">
			  
				
				</div>
				
			</div>
		</div>
	</div>
--->
</cfoutput>
<!--- ======================================= --->  
<cfinclude template="../_templates/footer.cfm">