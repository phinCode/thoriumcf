<cfprocessingdirective suppresswhitespace="yes">

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Create --->
<cfif isDefined('form.create')>

    <cfinvoke 
		component="#request.cfc#.users"
		method="create" 
		argumentcollection="#form#"
        returnvariable="result">
	</cfinvoke>
    
    <cfinvoke 
		component="#request.cfc#._messages"
		method="message" 
		value="#result.status#"
        returnvariable="result">
	</cfinvoke>
    
    <cfset session.show_message = #result#>
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Update --->
<cfif isDefined('form.update')>
here<cfabort></cfabort>
    <cfinvoke 
		component="#request.cfc#.users"
		method="update" 
		argumentcollection="#form#"
        returnvariable="result">
	</cfinvoke>
    
    <cfinvoke 
		component="#request.cfc#._messages"
		method="message" 
		value="#result.status#"
        returnvariable="result">
	</cfinvoke>
    
    <!--- setting these values will allow the update page trigger to be viewed --->
	<cfset session.show_message = #result#>
	    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Delete --->
<cfif isDefined('form.delete')>

    <cfinvoke 
		component="#request.cfc#.ads"
		method="delete" 
		argumentcollection="#form#"
        returnvariable="result">
	</cfinvoke>
    
    <cfinvoke 
		component="#request.cfc#._messages"
		method="message" 
		value="#result.status#"
        returnvariable="result">
	</cfinvoke>
    
    <cfset session.show_message = #result#>
    <cflocation url="view.cfm" addtoken="no">
    <cfabort>
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ---> 

    <cfif isDefined('url.view_record')> 
    
		<!--- get specific ads Records --->
        <cfinvoke 
            component="#request.cfc#.ads"
            method="read"
            ad_id = '#url.view_record#'
            returnvariable="records_ads">
        </cfinvoke>   
    
    <cfelse>
 
 		<!--- get all User Records --->
        <cfinvoke 
            component="#request.cfc#.users"
            method="read"
            returnvariable="records_users">
        </cfinvoke>     

    </cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

</cfprocessingdirective>