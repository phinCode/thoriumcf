<cfoutput>
<cfinclude template="controller.cfm">
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>CMS Log in</title>
	<!--- TELL THE BROWSER TO BE RESPONSE WITH THE SCREEN --->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- BOOTSTRAP CDN --->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<!-- FONT AWESONR CDN --->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
    
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						
						<h2 class="card-title text-center">Sign In</h2>
						
						<cfif isDefined('show_message')>#show_message#</cfif> <!--- SET IN CONTROLER.CFM --->
						
						<form class="form-signin" action="#cgi.script_name#" method="post">
							<div class="form-label-group mb-3">
								<input name="u_email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
							</div>

							<div class="form-label-group mb-3">
								<input name="u_password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
							</div>
							
							<input name="login" type="hidden" value="1">
							<button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!--- BOOTSTRAP CDN --->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
 
</body>
</html>
</cfoutput>
