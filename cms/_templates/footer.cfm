<cfoutput>
	<footer class="footer">
		<span class="text-right">
		Copyright <a target="_blank" href="##">Your Website</a>
		</span>
		<span class="float-right">
		Powered by <a target="_blank" href="https://www.pikeadmin.com"><b>Pike Admin</b></a>
		</span>
	</footer>

</div>
<!-- END main -->

	<!--- JS --->
<script src="#request.cmsURL#assets/js/modernizr.min.js"></script>
<script src="#request.cmsURL#assets/js/jquery.min.js"></script>
<script src="#request.cmsURL#assets/js/moment.min.js"></script>

<script src="#request.cmsURL#assets/js/popper.min.js"></script>
<script src="#request.cmsURL#assets/js/bootstrap.min.js"></script>

<script src="#request.cmsURL#assets/js/detect.js"></script>
<script src="#request.cmsURL#assets/js/fastclick.js"></script>
<script src="#request.cmsURL#assets/js/jquery.blockUI.js"></script>
<script src="#request.cmsURL#assets/js/jquery.nicescroll.js"></script>
<script src="#request.cmsURL#assets/js/jquery.scrollTo.min.js"></script>
<script src="#request.cmsURL#assets/plugins/switchery/switchery.min.js"></script>

<!-- App js -->
<script src="#request.cmsURL#assets/js/pikeadmin.js"></script>
</cfoutput>
  </body>
</html>
