<cfoutput>	
<!--- [START] CONTROLLER CODE --->

	<!--- GET TEMP TABLE DATA FOR THIS USER --->
	<cfquery datasource="#request.DSN#" name="data_user_x_temp_table" >
		SELECT * FROM user_x_temp_table WHERE user_fk = '1'
	</cfquery>
	
	<!--- GET COLUMN NAMES FROM TEMP TABLE --->
	<cfloop query = "data_user_x_temp_table"> 	
		<cfquery datasource="#request.DSN#" name="data_table_column_names" >
			SELECT COLUMN_NAME
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_SCHEMA = 'thoriumcf' AND TABLE_NAME = '#data_user_x_temp_table.temp_table#';
		</cfquery>
		
		<!--- GET THE PRIMARY KEY COLUMN NAME --->
		<cfquery datasource="#request.DSN#" name="data_primary_key" >
			SHOW KEYS FROM #data_user_x_temp_table.temp_table# WHERE Key_name = 'PRIMARY';
		</cfquery>
	</cfloop>
	
	<cfset primary_key_column = "#data_primary_key.column_name#">
	
	<!--- SET NUMBER OF COLUMNS - WILL ALWAYS BE MINUS 1 TO ACCOUNT FOR THE ID 
	<cfset number_of_columns = #data_table_column_names.RecordCount# - 1>--->

	<!--- UPDATE SCRIPT --->
	<cfif isDefined('FORM.x_save')> 
   		
   		<cfloop index="i" list="#Form.FieldNames#" delimiters=",">
    		
    		<cfif #i# does not contain "x_" and #i# does not contain "ID">
				<!--- UPDATE TABLE VALUES AS LOOPING THROUGH RECORDS --->
				<cfquery datasource="#request.DSN#" name="save_table_values" > 
					UPDATE #data_user_x_temp_table.temp_table#
					   SET #i# = '#Form[i]#'
					 WHERE #data_primary_key.column_name# = '#FORM.ID#'
				</cfquery>
			</cfif>

		</cfloop>
		
		<cfset row_updated = TRUE>
		
	</cfif>
	
	<!--- --->	
	<cfquery datasource="#request.DSN#" name="data_temp_table" >
		SELECT * FROM #data_user_x_temp_table.temp_table#
	</cfquery>
	
<!--- [END] CONTROLLER CODE --->
</cfoutput>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

<cfoutput>

<cfif isDefined ('row_updated')>
	<div class="alert alert-success" role="alert">
	  Row Updated
	</div>
</cfif>

<table class="table table-striped table-dark">

	<thead>
		<tr>
		<!--- SHOW COLUMN HEADERS --->
		<cfloop query="data_table_column_names">
			<cfif #data_table_column_names.column_name# does not contain "_ID">
				<th>#data_table_column_names.column_name#</th>
			</cfif>
		</cfloop>
		</tr>
	</thead>
	<tbody>
		
		
		<cfloop query="data_temp_table">
			<form action="table.cfm" method="post">
				<tr>
					<cfloop index="col" list="#columnlist#">					
						<cfif #col# does not contain "_ID">
							<th><input name="#col#" type="text" value="#data_temp_table[col][currentRow]#"></th>
						<cfelse>
							<input name="ID" type="hidden" value="#data_temp_table[col][currentRow]#">
						</cfif>
					</cfloop>
					<th>
						<input type="submit" name="x_save" value="Save" class="btn btn-primary btn-sm">
					</th>
				</tr>
			</form>
		</cfloop>
		
		
	</tbody>

</table>
</cfoutput>


<!---
<hr />
<br />
		<cfoutput query="data_temp_table"> 
			<hr />
			Current Row = #currentrow# 
			<br />
			<cfloop index="col" list="#columnlist#"> 
				<cfif #col# contains "_ID">
					<cfset id_name = #col#>
					<cfset id_value = #data_temp_table[col][currentRow]#>
				<cfelse>
					<form action="table.cfm" method="post">
						Column : #col# -- Value: <input name="#col#" value="#data_temp_table[col][currentRow]#">
						<br />
						<input type="hidden" name="x_#id_name#" value="#id_value#">
						<input type="hidden" name="x_table_name" value="#data_user_x_temp_table.temp_table#">
						<input type="submit" name= "x_save" value="Save">
					</form>
				</cfif> 
				<hr />
			</cfloop>


</cfoutput>--->
<!---
	<table border="1">
		<tr>
			<cfloop query="data_table_column_names">
				<cfif #data_table_column_names.column_name# does not contain "_id">
					<th>#data_table_column_names.column_name#</th>
				</cfif>
			</cfloop>
		</tr>
		<cfloop query="data_temp_table">
		#QueryGetRow(myQuery, 1)#
			<tr>
				<cfloop query="data_table_column_names">
					<cfif #data_table_column_names.column_name# does not contain "_id">
						
					</cfif>
				</cfloop>
			</tr>					
		</cfloop>
	</table>
--->

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>


