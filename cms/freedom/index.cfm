<cfset showForm = true>
<cfif structKeyExists(form, "xlsfile") and len(form.xlsfile)>

	<!--- Destination outside of web root --->
	<cfset dest = getTempDirectory()>

	<cffile action="upload" destination="#dest#" filefield="xlsfile" result="upload" nameconflict="makeunique">

	<cfif upload.fileWasSaved>
		<cfset theFile = upload.serverDirectory & "/" & upload.serverFile>
		<cfif isSpreadsheetFile(theFile)>
			<cfspreadsheet action="read" src="#theFile#" query="data" headerrow="1">
			<cffile action="delete" file="#theFile#">
			<cfset showForm = false>
		<cfelse>
			<cfset errors = "The file was not an Excel file.">
			<cffile action="delete" file="#theFile#">
		</cfif>
	<cfelse>
		<cfset errors = "The file was not properly uploaded.">	
	</cfif>
		
</cfif>

<cfif showForm>

	<cfif structKeyExists(variables, "errors")>
		<cfoutput>
		<p>
		<b>Error: #variables.errors#</b>
		</p>
		</cfoutput>
	</cfif>
	
	<form action="index.cfm" enctype="multipart/form-data" method="post">
		  
		  <input type="file" name="xlsfile" required>
		  <input type="submit" value="Upload XLS File">
		  
	</form>
	
<cfelse>

	<style>
	.ssTable { width: 100%; 
			   border-style:solid;
			   border-width:thin;
	}
	.ssHeader { background-color: #ffff00; }
	.ssTable td, .ssTable th { 
		padding: 10px; 
		border-style:solid;
		border-width:thin;
	}
	</style>
	
	<p>
		Here is the data in your Excel sheet (assuming first row as headers):
	</p>
	
	<cfset metadata = getMetadata(data)>
	<cfset colList = "">
	<cfloop index="col" array="#metadata#">
		<cfset colList = listAppend(colList, col.name)>
	</cfloop>
	
	<cfif data.recordCount is 1>
		<p>
			This spreadsheet appeared to have no data.
		</p>
	<cfelse>
	
	<cfset columns = ''>
	<cfset values = ''>
	<cfset array_columns = []>
	<cfset array_values = []>
	
		<table class="ssTable">
			<tr class="ssHeader">
				<cfloop index="c" list="#colList#">
					<cfoutput>
						<th>#c#</th>
						<!--- create column values --->
						<cfset columns = #columns# & "," & #c#>
       					<cfscript>
       						ArrayAppend(array_columns,[#columns#],"false");
       					</cfscript>
					</cfoutput>
				</cfloop>
			</tr>
			<cfoutput query="data" startRow="2">
				<tr>
				<cfloop index="c" list="#colList#">
					<td>
						#data[c][currentRow]#
						<cfset values = #values# & "," & #data[c][currentRow]#>
						<cfscript>
							ArrayAppend(array_values,[#values#],"false");
						</cfscript>
					</td>
				</cfloop>
				</tr>					
			</cfoutput>
		</table>
	</cfif>
	
	<cfoutput>

	<!--- CREATE EMPTY CONTAINER FOR COLUMNS/VALUES TO ADD TO SQL QUERY --->
	<!--- ============================================================= --->
		<cfset sql_create_columns = "">
		<cfset sql_insert_columns = "">
		<cfset sql_insert_values = "">

	<!--- LOOP OVER THE SHEET COLUMN NAMES TO BUILD THE SQL QUERY --->
	<!--- ======================================================= --->
		<cfset i_columns = 1>
		<cfloop list="#columns#" index="ListColumns" delimiters=",;">
			<cfset sql_create_columns = #sql_create_columns# &""& #ListColumns# &" VARCHAR(100) NULL,">
			<cfset sql_insert_columns = #sql_insert_columns# &""& #ListColumns#>
			<!--- ADD COMMA AFTER COLUMNS EXCEPT FOR LAST ONE --->
			<cfif #ListLen(columns)# neq #i_columns#>
				<cfset sql_insert_columns = #sql_insert_columns# &",">
				<cfset i_columns = #i_columns# + 1>
			</cfif>
		</cfloop>

	<!--- LOOP OVER THE SHEET ROW VALUES TO BUILD THE SQL QUERY --->
	<!--- ===================================================== --->
		<cfset i_values = 1>
		<cfloop list="#values#" index="ListValues" delimiters=",;">
			<cfif #i_values# eq 1>
				<!--- IF FIRST RECORD, I ADD " ( " AND THE VALUE TO THE STRING WITH A COMMA AT THE END --->
				<cfset sql_insert_values = #sql_insert_values# & "('" & #ListValues# & "'," />
				<cfset i_values = #i_values# + 1><!--- INCREMENT TO NEXT RECORDS --->
			<cfelseif #ListLen(columns)# eq #i_values#>
				<!--- IF THE LAST RECORD, I ADD COMMA, THE VALUE THEN THE ENDING SQL SYNTAX " ), " ---> 
				<cfset sql_insert_values = #sql_insert_values# & "'" & #ListValues# & "')," />
				<cfset i_values = 1><!--- INCREMENT TO NEXT RECORDS --->
			<cfelse>
				<!--- ALL RECORDS BETWEEN FIRST AND LAST --->
				<cfset sql_insert_values = #sql_insert_values# & "'" & #ListValues# & "'," />
				<cfset i_values = #i_values# + 1>
			</cfif>
		</cfloop>

	<!--- CREATE RANDOM VALUE FOR THE TEMP TABLE --->
	<!--- ====================================== --->
		<cfset tbl_name = RandRange('1000', '99999')>

	<!--- CREATE THE TEMP TABLE BASED ON UPLOADED SHEET --->
	<!--- ============================================= --->
		<cftry>
			<cflock name="create_table" type="exclusive" timeout="5">
				<cfquery name="create" datasource="#request.DSN#" result="create_table_result" >
					CREATE TABLE `thoriumcf`.`temp_#tbl_name#` ( `temp_#tbl_name#_id` INT NOT NULL , #sql_create_columns# PRIMARY KEY (`temp_#tbl_name#_id`)) ENGINE = InnoDB;
				</cfquery> 
			</cflock>	
			<cfcatch type="any">

				<cfset result.status = "error_creating">

				<cfif #request.debug# eq "On">
					<cfrethrow />
				</cfif>

			</cfcatch>
		</cftry>
	<!--- ALTER TABLE TO MAKE THE ID PRIMARY AND KEY --->
	<!--- ========================================== --->	
		<cftry>
			<cflock name="add_to_table" type="exclusive" timeout="5">
				<cfquery name="add" datasource="#request.DSN#" result="add_to_table_result" >
					ALTER TABLE `temp_#tbl_name#` CHANGE `temp_#tbl_name#_id` `temp_#tbl_name#_id` INT(11) NOT NULL AUTO_INCREMENT;
				</cfquery> 
			</cflock>	
			<cfcatch type="any">

				<cfset result.status = "error_creating">

				<cfif #request.debug# eq "On">
					<cfrethrow />
				</cfif>

			</cfcatch>
		</cftry>

	<!--- INSERT THE VALUES INTO THE TEMPORARY TABLE --->
	<!--- ========================================== --->
		<!--- REMOVE COMMA FROM END OF STRING --->
		<cfset sql_insert_values = REReplace(sql_insert_values, ",$", "")>

		<cftry>
			<cflock name="add_to_table" type="exclusive" timeout="5">	
				<cfquery name="insert" datasource="#request.DSN#" result="insert_values_result" >
					<!--- PreserveSingleQuotes REQUIRED OR WILL END UP WITH " NOT ' --->
					INSERT INTO `thoriumcf`.`temp_#tbl_name#` (#sql_insert_columns#) VALUES #PreserveSingleQuotes(sql_insert_values)#;
				</cfquery> 
			</cflock>	
			<cfcatch type="any">

				<cfset result.status = "error_creating">

				<cfif #request.debug# eq "On">
					<cfrethrow />	
				</cfif>

			</cfcatch>
		</cftry>

	<!--- INSERT CONNECTION FROM USER TO THE TEMP TABLE --->
	<!--- ============================================= --->
		<cftry>
			<cflock name="insert_connection" type="exclusive" timeout="5">	
				<cfquery name="insert" datasource="#request.DSN#" result="insert_connection_result" >
					<!--- PreserveSingleQuotes REQUIRED OR WILL END UP WITH " NOT ' --->
					INSERT INTO `thoriumcf`.`user_x_temp_table` (user_fk,temp_table) VALUES ('1','temp_#tbl_name#');
				</cfquery> 
			</cflock>	
			<cfcatch type="any">

				<cfset result.status = "error_creating">

				<cfif #request.debug# eq "On">
					<cfrethrow />	
				</cfif>

			</cfcatch>
		</cftry>		

	</cfoutput>
	
</cfif>