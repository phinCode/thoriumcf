<cfprocessingdirective suppresswhitespace="yes">

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- display message --->
<cfif isDefined('session.msg')>

    <cfinvoke 
		component="#request.cfc#._messages"
		method="message" 
		value = "#session.msg#"
        returnvariable="result">
	</cfinvoke>
    
    <cfset show_message = #result#>
    <cfset session.msg = "FALSE">
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Log in admin --->
<cfif isDefined('form.login')>

    <cfif #form.u_email# neq "" and #form.u_password# neq "">
        
        <cfset form.redirect_success = "dashboard.cfm">
		<cfset form.redirect_failure = "index.cfm">
        
        <cfinvoke 
            component="#request.cfc#.users"
            method="login" 
            argumentcollection="#form#"
            returnvariable="result">
        </cfinvoke>
        
        <cfdump var="#result#"></cfdump>
        <cfabort></cfabort>
        <!--- add role validation here for Admin's only --->
        
        <!--- --->
   
    </cfif>
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Recover Password --->
<cfif isDefined('form.recover_password')>

    <cfinvoke 
		component="#request.cfc#.admin"
		method="recover_password" 
		argumentcollection="#form#"
        returnvariable="result">
	</cfinvoke>
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Create Adim --->
<cfif isDefined('form.create')>

    <cfinvoke 
		component="#request.cfc#.users"
		method="create" 
		argumentcollection="#form#"
        returnvariable="result">
	</cfinvoke>
    
    <cfinvoke 
		component="#request.cfc#._messages"
		method="message" 
		value = "#result.status#"
        returnvariable="result">
	</cfinvoke>
    
    <cfset show_message = #result#>
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Update Adim --->
<cfif isDefined('form.update')>

    <cfinvoke 
		component="#request.cfc#.admin"
		method="update" 
		argumentcollection="#form#"
        returnvariable="result">
	</cfinvoke>
    
    <cfinvoke 
		component="#request.cfc#._messages"
		method="message" 
		value = "#result.status#"
        returnvariable="result">
	</cfinvoke>
    
    <cfset show_message = #result#>
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

<!--- Delete Adim --->
<cfif isDefined('form.delete')>

    <cfinvoke 
		component="#request.cfc#.admin"
		method="delete" 
		argumentcollection="#form#"
        returnvariable="result">
	</cfinvoke>
    
    <cfinvoke 
		component="#request.cfc#._messages"
		method="message" 
		value = "#result.status#"
        returnvariable="result">
	</cfinvoke>
    
    <cfset show_message = #result#>
    
</cfif>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

	<!--- get all Admin Records --->
    <cfinvoke 
		component="#request.cfc#.admin"
		method="read"
        returnvariable="records_Admin">
	</cfinvoke>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

</cfprocessingdirective>