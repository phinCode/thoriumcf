<cfcomponent displayname="Users Table" hint="">

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	
	<cffunction name="create" returntype="struct">

        <cfargument name="u_name_fist" type="string" required="no" default="">
        <cfargument name="u_name_last" type="string" required="no" default="">
        <cfargument name="u_password" type="string" required="no" default="">
        <cfargument name="u_email" type="string" required="no" default="">
        <cfargument name="u_created_at" type="string" required="no" default="#DateFormat(now(), "yyyy-mm-dd")#">
        							
		<!--- result structure --->
        <cfset result = structNew()>
        <cfset result.status = "created">
		
        <!--- create password if one was not passed --->
        <cfif #arguments.u_password# eq "">
            <cfinvoke 
                component="#request.cfc#._helpers"
                method="random_string_Helpers" 
                returnvariable="temp_password">
            </cfinvoke>
		</cfif>
        
        
		<cftry>
			<cflock name="add_to_table" type="exclusive" timeout="5">
            	
				<cfquery datasource="#request.DSN#" >
                    INSERT INTO admin
                    (u_name_first, u_name_last, u_email, u_password, created_at)
                    VALUES
                    (
					<cfqueryparam value = "#arguments.u_name_first#" cfsqltype = "cf_sql_varchar">,
					<cfqueryparam value = "#arguments.u_name_last#" cfsqltype = "cf_sql_varchar">,
					<cfqueryparam value = "#arguments.u_email#" cfsqltype = "cf_sql_varchar">,
					<cfqueryparam value = "#temp.password#" cfsqltype = "cf_sql_varchar">,
					<cfqueryparam value = "#arguments.u_created_at#" cfsqltype = "cf_sql_varchar">
                     );
				</cfquery> 	
                
			</cflock>	
				<cfcatch type="any">
					<cfset result.status = "error_creating">
					
                    <cfif #request.debug# eq "On">
                        <cfrethrow />
                    </cfif>
                    
                </cfcatch>
		</cftry>

        <cfreturn result>
        
	</cffunction>
    
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	
	<cffunction name="read" returntype="query">
		
        <cfargument name="user_id" type="string" required="no" default="">
        <cfargument name="u_name_fist" type="string" required="no" default="">
        <cfargument name="u_name_last" type="string" required="no" default="">
        <cfargument name="u_email" type="string" required="no" default="">
        <cfargument name="u_password" type="string" required="no" default="">
        
        <cfquery name="query" datasource="#request.DSN#">
			
            SELECT u.user_id, u.u_name_first, u.u_name_last, u.u_email, u.u_password
			  FROM users u
             WHERE 0=0
			 <cfif #arguments.user_id# neq "">
               AND u.user_id = <cfqueryparam value = "#arguments.user_id#" cfsqltype = "cf_sql_integer"> 
             </cfif>
             
			 <cfif #arguments.u_email# neq "">
               AND u.u_email = <cfqueryparam value = "#arguments.u_email#" cfsqltype = "cf_sql_varchar">  
             </cfif>
             
			 <cfif #arguments.u_password# neq "">
               AND u.u_password = <cfqueryparam value = "#arguments.u_password#" cfsqltype = "cf_sql_varchar"> 
             </cfif>
                           
		</cfquery>

		<cfreturn query>
        
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->		

	<cffunction name="update" returntype="struct">
			
        <cfargument name="user_id" type="numeric" required="no" default="">
        <cfargument name="u_name_fist" type="string" required="no" default="">
        <cfargument name="u_name_last" type="string" required="no" default="">
        <cfargument name="u_email" type="string" required="no" default="">
        <cfargument name="u_password" type="string" required="no" default="">
			
		<!--- result structure --->
        <cfset result = structNew()>
        <cfset result.status = "updated">
			
        <cftry>
            <cflock name="update_table" type="exclusive" timeout="5">
                
                <cfquery datasource="#request.DSN#">
                    UPDATE users
                       SET 
                       		u_name_first = <cfqueryparam value = "#arguments.u_name_first#" cfsqltype = "cf_sql_varchar">,
                            u_name_last = <cfqueryparam value = "#arguments.u_name_last#" cfsqltype = "cf_sql_varchar">,
                     WHERE user_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.user_id#">;
                </cfquery>
                
            </cflock>

            <cfcatch type="any">
                    <cfset result.status = "error_updating">
                </cfcatch>
        </cftry>

		<cfreturn result>
        
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	

	<cffunction name="delete" returntype="struct">
							
		<!--- result structure --->
        <cfset result = structNew()>
        <cfset result.status = "deleted">

		<cftry>
			<cflock name="delete_to_table" type="exclusive" timeout="5">	
				
                <cfquery datasource="#request.DSN#" >
                    DELETE FROM users
                     WHERE user_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.user_id#"> 
				</cfquery> 
                	
			</cflock>	
				<cfcatch type="any">
					<cfset result.status = "error_deleting">
				</cfcatch>
		</cftry>	
		
        <cfreturn result>
        
	</cffunction>
    
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->			

	<cffunction name="login" returntype="any">

        <cfargument name="u_email" type="string" required="yes" >
        <cfargument name="u_password" type="string" required="yes" >
        <cfargument name="redirect_success" type="string" required="no" >
        <cfargument name="redirect_failure" type="string" required="no" >
       	<cfargument name="current_location" type="string" required="no" >
        							
		<!--- result structure --->
        <cfset result = structNew()>
        <cfset result.status = "login_success">
		
		<cfinvoke 
			component="#request.cfc#.users"
			method="read" 
            u_email = "#arguments.u_email#"
            u_password = "#arguments.u_password#"
			returnvariable="result">
		</cfinvoke>
	
    	<cfoutput>
        	
			<cfif #result.recordcount# eq 1>
				<cfset SESSION.Auth = StructNew()>
                <cfset SESSION.Auth.is_logged_in = "Yes">
                <cfset SESSION.Auth.id = result.user_id>
                <cfset SESSION.Auth.u_name_first = result.u_name_first>
                <cfset SESSION.Auth.u_name_last = result.u_name_last>
                <cfset SESSION.Auth.u_email = result.u_email>
                
                <!--- get users ip address --->
                <cfinvoke 
                    component="#request.cfc#._helpers"
                    method="user_ip_address_Helpers" 
                    returnvariable="ip_address">
                </cfinvoke>				

                <cfquery name="uu" datasource="#request.DSN#">
                    UPDATE users
                       SET 
                            last_login = <cfqueryparam cfsqltype="cf_sql_varchar" value="#DateFormat(now(), "yyyy-mm-dd HH:mm:ss")#"> ,
                            ip_address = <cfqueryparam cfsqltype="cf_sql_varchar" value="#ip_address#">
                     WHERE user_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#result.user_id#">;
                </cfquery>               
                
                <!--- only Admin allowed in CMS 
        		<cfif #arguments.current_location# eq 'cms' and #SESSION.Auth.a_priv# neq '1'>
        			<cflocation url="#request.siteURL#cms/index.cfm?msg=logged_out" addtoken="no">
				</cfif>--->
                
                <cflocation url="#arguments.redirect_success#" addtoken="no">
                		
            <cfelse>

                <!--- login failed --->
                <cfset session.msg = "invalid_credentials">
                <cflocation url="#arguments.redirect_failure#" addtoken="no">
                
            </cfif>
            
		</cfoutput>
        
        <cfreturn result>
        
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

	<cffunction name="recover_password" returntype="any">

        <cfargument name="a_email" type="string" required="yes" >
        <cfargument name="redirect_success" type="string" required="no" >
        <cfargument name="redirect_failure" type="string" required="no" >
                							
		<!--- result structure --->
        <cfset result = structNew()>
        <cfset result.status = "password_sent">

		<cfinvoke 
			component="#request.cfc#.admin"
			method="read" 
            a_email = "#arguments.a_email#"
			returnvariable="result">
		</cfinvoke>
	
    	<cfoutput>
        	
			<cfif #result.recordcount# eq 1>
				
                <cfmail to="#result.a_email#"
                		from="noReply@NeaseCrossCountry.com"
                        subject="NeaseCrossCountry.com Password Help"
                        type="html">
                <h3>Your crendentials for #request.title#</h3>
                <p>Your password is #result.a_pswd#</p>
                <p>&nbsp;</p>
                Site: #request.siteUrl#
                </cfmail>
                
                <cfset session.msg = "password_sent">
                <cflocation url="#arguments.redirect_success#" addtoken="no">
                        
            <cfelse>
                 
                <cfset session.msg = "invalid_email">          
                <cflocation url="#arguments.redirect_failure#" addtoken="no">
                
            </cfif>
            
		</cfoutput>
        
        <cfreturn result>
        
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->

</cfcomponent>

